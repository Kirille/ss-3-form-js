'use strict';

function Mediator () {
	var listeners = [];

	this.addEventListener = function (fn) {
		listeners.push(fn);
	};

	this.trigger = function (data) {
		listeners.forEach(function (listener) {
			listener(data);
		});
	};
}

window.mediator = new Mediator();