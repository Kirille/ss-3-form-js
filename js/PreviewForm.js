'use strict';

function PreviewForm (parameters) {
    var el = document.createElement('div');

    initialize();

    function initialize () {
        parameters.caption = 'preview';

        el.innerHTML = createPreviewForm(parameters.data, parameters.eventName);
        
        addEvent();
    }

    function switchForm (event) {
        event.preventDefault();

        clearEvent();
        
        mediator.trigger(parameters);
    }

    function addEvent() {
        if (!parameters.elements.button) {
            parameters.elements.button = el.querySelector('#' + parameters.eventName);
            parameters.elements.button.addEventListener('click', switchForm, false);
        }
    }

    function clearEvent () {
        if (parameters.elements.button) {
            parameters.elements.button.removeEventListener('click', switchForm, false);
            parameters.elements.button = '';
        }
    }

    this.getElement = function () {
        return el;
    };

	return this;
}