'use strict';

function EditForm (parameters) {
    var el = document.createElement('div');

    initialize();

    function initialize () {
        parameters.caption = 'edit';

        el.innerHTML = createInputForm(parameters.data, parameters.eventName);
        
        addEvent();
    }

    function switchForm (event) {
        event.preventDefault();

        setPersonInformation();
        
        clearEvent();
        
        mediator.trigger(parameters);
    }

    function addEvent() {
        if (!parameters.elements.button) {
            parameters.elements.button = el.querySelector('#' + parameters.eventName);
            parameters.elements.button.addEventListener('click', switchForm, false);
        }
    }

    function clearEvent () {
        if (parameters.elements.button) {
            parameters.elements.button.removeEventListener('click', switchForm, false);
            parameters.elements.button = '';
        }
    }

    function setPersonInformation () {
        var inputName = el.querySelector('#inputName').value || '',
            inputPhone = el.querySelector('#inputPhone').value || '',
            inputEmail = el.querySelector('#inputEmail').value || '';

        parameters.data.set('name', inputName);
        parameters.data.set('phone', inputPhone);
        parameters.data.set('email', inputEmail);
    }

    this.getElement = function () {
        return el;
    };

	return this;
}