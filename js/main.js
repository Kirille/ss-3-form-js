/*
*	About project
*		This project create form and can preview this form.
* 		It can be switched by click.
*
*	About parameters
*		data - it is space where you can collect your information
*		eventName - is is ID for our button
*		elements - contains our DOM
*
*	Last update: 7/05/2016
*/
'use strict';

document.addEventListener('DOMContentLoaded', init, false);

function init () {
	new Controller();
}