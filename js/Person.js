'use strict';

function Person (_name, _phone, _email) {
	var attr = {
		name: _name,
		phone: _phone,
		email: _email
	};

	this.set = function (id, value) {
		attr[id] = value;
	};

	this.get = function (value) {
		return attr[value];
	};

	this.toJSON = function () {
		return attr;
	};

	return this;
}