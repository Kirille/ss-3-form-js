'use strict';

function Controller () {
	var parameters = {
			data: new Person(),
			eventName: 'btn-event',
			caption: '',
			elements: {
				container: document.getElementById('container'),
				button: ''
			},
			view: {
				edit: '',
				preview: ''
			}
		};

	init();

	function init () {
		mediator.addEventListener(changeView);

		parameters.view.edit = new EditForm(parameters);

		parameters.elements.container.appendChild(parameters.view.edit.getElement());
	}

	function changeView (data) {
		parameters.elements.container.innerHTML = '';

		if (data.caption === 'edit') {
			parameters.view.preview = new PreviewForm(parameters);
			parameters.elements.container.appendChild(parameters.view.preview.getElement())
		} else {
			parameters.view.edit = new EditForm(parameters);
			parameters.elements.container.appendChild(parameters.view.edit.getElement())
		}
	}
}