'use strict';

function createInputForm (data, eventId) {
	var valid = checkInformation(data),
		form = '';

	form += '<form>';
	form += createInput('text', 'inputName', 'inputName', 'Enter name','Name', valid.name);
	form += createInput('number', 'inputPhone', 'inputPhone', 'Enter phone', 'Phone', valid.phone);
	form += createInput('email', 'inputEmail', 'inputEmail', 'Enter email','Email', valid.email);
	form += createInput('submit', '', eventId, '', '', 'Preview');
	form += '</form>';

	return form;
}

function createPreviewForm (person, eventId) {
	var form = '';

	form += createText(person);
	form += createInput('submit', '', eventId, '', '', 'Edit');

	return form;
}

function createText (person) {
	var tpl = '<div>Name: :name</div><div>Phone: :phone</div><div>Email: :email</div>';

	return template(tpl, person.toJSON());
}

function createInput (_type, _name, _id, _placeholder, _label, _value) {
	var	tpl = '<label>:label <input type=":type" name=":name" id=":id" value=":value" placeholder=":placeholder"></label>',
		convertKeys = {};

	convertKeys = {
		type: _type,
		name: _name,
		id: _id,
		value: _value,
		placeholder: _placeholder,
		label: _label
	};

	return template(tpl, convertKeys);
}

function checkInformation (data) {
	return {
		name: data.get('name') || '',
		phone: data.get('phone') || '',
		email: data.get('email') || ''
	};
}

function template (tpl, data) {
	for(var key in data) {
		tpl = tpl.replace(':' + key, data[key]);
	}

	return tpl;
}